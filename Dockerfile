# FROM node:13-alpine

# EXPOSE 8080
# WORKDIR /app

# COPY ./build/ .
# COPY ./entrypoint.sh .

# ENTRYPOINT [ "sh", "entrypoint.sh" ]

FROM nginx:alpine

EXPOSE 80

WORKDIR /usr/share/nginx/html

COPY ./build/ .
COPY ./entrypoint.sh .

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

ENTRYPOINT [ "sh", "entrypoint.sh" ]

CMD ["nginx", "-g", "daemon off;"]

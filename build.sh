#!/bin/bash
BUILD_ID=`git rev-parse --short HEAD`
npm run build

docker buildx build --platform linux/arm64/v8,linux/amd64 --pull -f Dockerfile -t radicand/home-gardener-frontend:latest -t radicand/home-gardener-frontend:$BUILD_ID --push .
#docker buildx build --platform linux/arm64/v8,linux/amd64 --pull -f Dockerfile.multiarch -t radicand/home-gardener-frontend:latest -t radicand/home-gardener-frontend:$BUILD_ID --push .

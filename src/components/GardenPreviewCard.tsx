import Avatar from '@material-ui/core/Avatar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import { red } from '@material-ui/core/colors';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { MyGardensQuery } from '../generated/graphql';
import history from '../utils/history';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
    chip: {
      marginRight: theme.spacing(1),
    },
  }),
);

type IProps = MyGardensQuery['gardens'][0];

export default function GardenPreviewCard(props: IProps) {
  const classes = useStyles();
  // const [expanded, setExpanded] = React.useState(false);

  // const handleExpandClick = () => {
  //   setExpanded(!expanded);
  // };

  return (
    <>
      <Card
        className={classes.card}
        onClick={(ev) => {
          ev.preventDefault();
          history.push(`/garden/${props.id}`);
        }}
        style={{ cursor: 'pointer' }}
      >
        <CardHeader
          avatar={
            <Avatar aria-label="garden" className={classes.avatar}>
              {props.name[0]}
            </Avatar>
          }
          // action={
          //   <IconButton aria-label="settings">
          //     <MoreVertIcon />
          //   </IconButton>
          // }
          title={props.name}
        />
        {/* {props.photo && (
          <CardMedia
            className={classes.media}
            image={`data:image/jpg;base64,${props.photo}`}
            title={props.title}
          />
        )} */}
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            {props.description}
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          {/* <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton> */}
          {/* <IconButton aria-label="share">
          <ShareIcon />
        </IconButton> */}
          {/* <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton> */}
        </CardActions>
      </Card>
    </>
  );
}

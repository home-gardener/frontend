import { createStyles, makeStyles, Theme, Typography } from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import React from 'react';
import {
  GardenRole,
  MyGardensDocument,
  MyGardensQuery,
  useCreateOneGardenMutation,
  useMeQuery,
  useMyGardensQuery,
} from '../generated/graphql';
import { useAuth0 } from '../react-auth0-spa';
import history from '../utils/history';
import MyGardenList from './MyGardenList';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    fab: {
      position: 'fixed',
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
  }),
);

const Home = () => {
  const classes = useStyles();
  const { user } = useAuth0();
  const [createOneGarden] = useCreateOneGardenMutation();
  const { loading: meLoading, data: meData } = useMeQuery();
  const { loading, error, data } = useMyGardensQuery({
    variables: {
      userId: meData?.me?.id || '',
    },
  });

  const addGarden = async () => {
    if (!meData?.me?.id) {
      return;
    }

    const gardenData = await createOneGarden({
      variables: {
        name: `New Garden ${+new Date()}`,
        memberships: {
          create: [
            {
              role: GardenRole.Admin,
              user: {
                connect: {
                  id: meData.me.id,
                },
              },
            },
          ],
        },
      },
      update: (store, { data }) => {
        if (!data || !meData?.me?.id) {
          return;
        }

        // Read the data from our cache for this query.
        const cacheData = store.readQuery<MyGardensQuery>({
          query: MyGardensDocument,
          variables: {
            userId: meData.me.id,
          },
        });

        if (!cacheData) {
          return;
        }

        // Add our comment from the mutation to the end.
        cacheData.gardens.push(data.createOneGarden);
        // Write our data back to the cache.
        store.writeQuery<MyGardensQuery>({
          query: MyGardensDocument,
          variables: {
            userId: meData.me.id,
          },
          data: cacheData,
        });
      },
    });

    history.push(`/garden/${gardenData.data?.createOneGarden.id}`);
  };

  if (loading) return <p>Loading...</p>;
  if (user && error) return <p>Error :( {JSON.stringify(error)}</p>;

  return (
    <>
      {user && data && (
        <>
          <Typography variant="h4" gutterBottom={true} color="textPrimary">
            My Gardens
          </Typography>
          <MyGardenList gardens={data?.gardens} />
        </>
      )}
      {!data && (
        <Typography variant="body1" gutterBottom={true} color="textPrimary">
          You are not part of any gardens at this time.
        </Typography>
      )}
      {user?.hasAuthorization('create:garden') && !meLoading && (
        <Fab
          color="primary"
          aria-label="edit"
          className={classes.fab}
          onClick={() => addGarden()}
        >
          <AddIcon />
        </Fab>
      )}
    </>
  );
};

export default Home;

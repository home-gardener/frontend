import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import * as React from 'react';
import { GardenPixelFieldsFragment } from '../generated/graphql';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    gardenPixel: {
      width: 20,
      height: 20,
      color: '#f3f3f3',
      border: '1px solid gray',
      // flex: 1,
      // flexGrow: 0,
      // flexShrink: 0,
    },
  }),
);

const GardenPixel = (
  props: Partial<GardenPixelFieldsFragment> & { onClick?: () => void },
) => {
  const classes = useStyles(props);
  return (
    <div
      className={classes.gardenPixel}
      title={`${props.x},${props.y}${
        props.plant ? `: ${props.plant?.name}` : ``
      }`}
      style={{ backgroundColor: props.color }}
      onClick={props.onClick}
    >
      &nbsp;
    </div>
  );
};

export default GardenPixel;

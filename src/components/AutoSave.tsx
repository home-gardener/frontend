import * as _ from 'lodash';
import { default as React, useEffect, useRef, useState } from 'react';
import { useFormState } from 'react-final-form';
import { Garden } from '../generated/graphql';

interface AutoSaveProps {
  save: (field: string, value: any, values: Garden) => Promise<void>;
}

const AutoSave = (props: AutoSaveProps) => {
  const values = useRef<Garden>();
  const previousActive = useRef<string>();

  const [submitting, setSubmitting] = useState<boolean>(false);
  const [promise, setPromise] = useState<Promise<void>>();

  const formState = useFormState({
    ...props,
    subscription: { active: true, values: true },
  });

  const save = async (blurredField: string) => {
    if (promise) {
      await promise;
    }

    if (
      _.get(formState.values, blurredField) !==
      _.get(values.current || {}, blurredField, null)
    ) {
      setSubmitting(true);

      setPromise(
        props.save(
          blurredField,
          _.get(formState.values, blurredField),
          formState.values as Garden,
        ),
      );
      values.current = formState.values as Garden;

      await promise;
      setPromise(undefined);
      setSubmitting(false);
    }
  };

  useEffect(() => {
    if (previousActive.current && previousActive.current !== formState.active) {
      save(previousActive.current);
    }
    previousActive.current = formState.active;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formState.active]);

  return submitting ? <div>Saving...</div> : <></>;
};

export default AutoSave;

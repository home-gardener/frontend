import React from 'react';
import { Route, RouteComponentProps } from 'react-router-dom';
import { useAuth0 } from '../react-auth0-spa';

const PrivateRoute = ({
  component: Component,
  path,
  ...rest
}: {
  component: React.ComponentType<any>;
  path: string;
  [key: string]: any;
}) => {
  const { isAuthenticated } = useAuth0();

  const render = (props: RouteComponentProps) =>
    isAuthenticated === true ? <Component {...props} /> : null;

  return <Route path={path} render={render} {...rest} />;
};

export default PrivateRoute;

import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import BreadCrumbs from './BreadCrumb';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: 32,
    flex: '1 1 100%',
    position: 'relative',
    maxWidth: '100%',
    margin: '0 auto',
    backgroundColor: `${theme.palette.background.default} !important`,
    minHeight: '100vh',
    maxHeight: '100%',
  },
}));

const Frame: React.SFC = ({ children }) => {
  const classes = useStyles();

  return (
    <Container className={classes.root} component="main" id="main-content">
      <BreadCrumbs />
      {children}
    </Container>
  );
};

export default Frame;

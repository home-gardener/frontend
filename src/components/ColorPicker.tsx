import { InputLabel } from '@material-ui/core';
import * as React from 'react';
import { CompactPicker } from 'react-color';

const ColorPicker = ({ input: { onChange, value }, label, ...rest }: any) => {
  return (
    <>
      <InputLabel>{label}</InputLabel>
      <CompactPicker onChange={onChange} />
    </>
  );
};

export default ColorPicker;

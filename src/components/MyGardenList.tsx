import Grid from '@material-ui/core/Grid';
import React from 'react';
import { MyGardensQuery } from '../generated/graphql';
import GardenPreviewCard from './GardenPreviewCard';

interface IProps {
  gardens?: MyGardensQuery['gardens'];
}

const MyGardenList = (props: IProps) => {
  if (!props.gardens) {
    return <></>;
  }

  return (
    <Grid container spacing={3}>
      {props.gardens?.map(({ __typename, ...garden }, index) => (
        <Grid item xs={12} sm={6} md={3} key={index}>
          <GardenPreviewCard {...garden} />
        </Grid>
      ))}
    </Grid>
  );
};

export default MyGardenList;

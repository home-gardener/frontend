import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { BreadCrumbContext } from '../context/BreadCrumbContext';
import LinkComponent from './LinkComponent';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    breadcrumb: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  }),
);

export default function BreadCrumbs() {
  const classes = useStyles();
  const [levels] = React.useContext(BreadCrumbContext);
  const useLevels = [...levels];
  const lastLevel = useLevels.pop();

  return (
    <Breadcrumbs aria-label="breadcrumb" className={classes.breadcrumb}>
      <Link color="inherit" href="/" to={'/'} component={LinkComponent}>
        Home
      </Link>
      {useLevels.map((level, index) => (
        <Link
          key={index}
          color="inherit"
          href={level.location}
          to={level.location}
          component={LinkComponent}
        >
          {level.name ?? '...'}
        </Link>
      ))}
      {lastLevel && (
        <Typography color="textPrimary">{lastLevel.name}</Typography>
      )}
    </Breadcrumbs>
  );
}

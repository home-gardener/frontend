import { ApolloProvider } from '@apollo/react-hooks';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { setContext } from 'apollo-link-context';
import { HttpLink } from 'apollo-link-http';
import React from 'react';
import ENV from '../env';
import { useAuth0 } from '../react-auth0-spa';

const AuthorizedApolloProvider: React.SFC = ({ children }) => {
  const { getTokenSilently, isAuthenticated } = useAuth0();

  const httpLink = new HttpLink({
    uri: ENV.REACT_APP_BACKEND_URL,
  });

  const authLink = setContext(async () => {
    if (isAuthenticated) {
      const token = await getTokenSilently(); // access token
      return {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
    } else {
      return {};
    }
  });

  const apolloClient = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
    connectToDevTools: true,
  });

  return <ApolloProvider client={apolloClient}>{children}</ApolloProvider>;
};

export default AuthorizedApolloProvider;

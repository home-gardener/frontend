import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { TextField } from 'mui-rff';
import * as React from 'react';
import { Form } from 'react-final-form';
import { GardenFieldsFragment } from '../generated/graphql';

interface EditGardenDetailsModalProps {
  open: boolean;
  handleClose: () => void;
  handleCommit: (values: GardenFieldsFragment) => void;
  initialValues: GardenFieldsFragment;
}

const EditGardenDetailsModal = (props: EditGardenDetailsModalProps) => {
  return (
    <Form
      initialValues={props.initialValues}
      onSubmit={props.handleCommit}
      // validate={validate}
      // subscription={{ values: true }}
      render={({ handleSubmit, values }) => {
        return (
          <Dialog
            open={props.open}
            onClose={props.handleClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">Edit details</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Edit details for this garden
              </DialogContentText>
              <TextField label="Garden name" name="name" required={true} />
              <TextField
                label="Description"
                name="description"
                required={false}
              />
              <TextField
                label="Notes"
                name="notes"
                required={false}
                multiline={true}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={props.handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={handleSubmit} color="primary">
                Save
              </Button>
            </DialogActions>
          </Dialog>
        );
      }}
    />
  );
};

export default EditGardenDetailsModal;

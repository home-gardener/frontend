import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Switches } from 'mui-rff';
// import { TextField } from 'mui-rff';
import * as React from 'react';
import { ColorResult } from 'react-color';
import { Field, Form } from 'react-final-form';
import ColorPicker from './ColorPicker';

export interface PassedPixel {
  x: number;
  y: number;
  id?: string;
  colorString?: string;
  color?: ColorResult;
  plantable?: boolean;
}

interface EditGardenPixelModalProps {
  open: boolean;
  handleClose: () => void;
  handleCommit: (values: PassedPixel) => void;
  initialValues?: PassedPixel;
}

const EditGardenPixelModal = (props: EditGardenPixelModalProps) => {
  return (
    <Form
      initialValues={props.initialValues}
      onSubmit={props.handleCommit}
      // validate={validate}
      // subscription={{ values: true }}
      render={({ handleSubmit, values }) => {
        return (
          <Dialog
            open={props.open}
            onClose={props.handleClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">Edit pixel</DialogTitle>
            <DialogContent>
              <DialogContentText>Edit details for this pixel</DialogContentText>
              <Field
                component={ColorPicker}
                color={values.colorString}
                label="Color"
                name="color"
                required={true}
              />
              <br />
              <Switches
                name="plantable"
                // label="Plantable"
                data={{ label: 'Plantable', value: true }}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={props.handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={handleSubmit} color="primary">
                Save
              </Button>
            </DialogActions>
          </Dialog>
        );
      }}
    />
  );
};

export default EditGardenPixelModal;

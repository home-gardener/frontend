import { Button, Typography } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import * as React from 'react';
import { useContext, useEffect, useState } from 'react';
import { BreadCrumbContext } from '../context/BreadCrumbContext';
import {
  GardenDocument,
  GardenFieldsFragment,
  GardenQuery,
  useGardenQuery,
  useUpdateOneGardenMutation,
  useUpsertOneGardenPixelMutation,
} from '../generated/graphql';
import range from '../utils/range';
import EditGardenDetailsModal from './EditGardenDetailsModal';
import EditGardenPixelModal, { PassedPixel } from './EditGardenPixelModal';
// import { useAuth0 } from '../react-auth0-spa';
import GardenPixel from './GardenPixel';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    gardenGridLayout: {
      overflow: 'scroll',
      display: 'flex',
      flexFlow: 'row wrap',
    },
    gardenGridContainer: {
      flex: '20',
    },
    gardenGrid: {
      width: '100%',
    },
    gardenRow: {
      display: 'flex',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    sideButton: {
      flexDirection: 'column',
      flex: '1',
    },
  }),
);

const Garden = (props: { match: any }) => {
  // const { user } = useAuth0();
  const classes = useStyles(props);
  const { loading, error, data } = useGardenQuery({
    variables: {
      id: props.match.params.id,
    },
  });
  const [updateOneGarden] = useUpdateOneGardenMutation();
  const [upsertOneGardenPixel] = useUpsertOneGardenPixelMutation();

  const [detailsModelOpen, setDetailsModelOpen] = useState<boolean>(false);
  const [pixelModalOpen, setPixelModalOpen] = useState<boolean>(false);
  const [selectedPixel, setSelectedPixel] = useState<PassedPixel>();

  const [, setLevels] = useContext(BreadCrumbContext);

  const saveDetails = async (values: GardenFieldsFragment) => {
    if (!data?.garden) {
      return;
    }

    await updateOneGarden({
      variables: {
        id: data.garden.id,
        name: values.name,
        description: values.description,
        notes: values.notes,
      },
      optimisticResponse: {
        updateOneGarden: {
          ...data.garden,
          name: values.name,
          description: values.description,
          notes: values.notes,
        },
      },
    });

    setDetailsModelOpen(false);
  };

  const saveGardenPixel = async (values: PassedPixel) => {
    if (!data?.garden) {
      return;
    }

    const { x, y } = values;

    await upsertOneGardenPixel({
      variables: {
        ...values,
        id: values.id ?? 'null',
        color: values.color?.hex ?? '#d3d3d3',
        plantable: values.plantable ?? false,
        x,
        y,
        gardenId: data.garden.id,
      },
      optimisticResponse: {
        upsertOneGardenPixel: {
          __typename: 'GardenPixel',
          id: values.id ?? 'TBD',
          color: values.color?.hex ?? '#d3d3d3',
          // harvestedDate: pixel.harvestedDate,
          plantable: values.plantable ?? false,
          // plant: pixel.plantUpdate?.connect?.id
          //   ? {
          //       id: pixel.plantUpdate.connect.id,
          //       name: 'TBD',
          //       daysToHarvest: 100,
          //     }
          //   : null,
          // plantedBy: pixel.plantedByUpdate?.connect?.id
          //   ? { id: pixel.plantedByUpdate.connect.id, name: '...' }
          //   : null,
          x,
          y,
        },
      },
      update: (store, { data: pixelData }) => {
        if (!pixelData || !data?.garden) {
          return;
        }
        if (
          data?.garden?.pixels.find(
            (pixel) =>
              pixel.x === pixelData.upsertOneGardenPixel.x &&
              pixel.y === pixelData.upsertOneGardenPixel.y,
          )
        ) {
          return;
        }
        // Read the data from our cache for this query.
        const cacheData = store.readQuery<GardenQuery>({
          query: GardenDocument,
          variables: {
            id: data.garden.id,
          },
        });

        if (!cacheData) {
          return;
        }

        // Add our comment from the mutation to the end.
        cacheData.garden?.pixels.push(pixelData.upsertOneGardenPixel);
        // Write our data back to the cache.
        store.writeQuery<GardenQuery>({
          query: GardenDocument,
          variables: {
            id: data.garden.id,
          },
          data: cacheData,
        });
      },
    });

    setPixelModalOpen(false);
  };

  const increaseGardenSize = (
    field: keyof Pick<
      GardenFieldsFragment,
      'topSize' | 'bottomSize' | 'leftSize' | 'rightSize'
    >,
  ) => {
    if (!data?.garden) {
      return;
    }

    const fieldValue = data.garden[field] + (data.garden[field] < 0 ? -1 : 1);

    updateOneGarden({
      variables: {
        id: data.garden.id,
        [field]: fieldValue,
      },
      optimisticResponse: {
        updateOneGarden: {
          ...data.garden,
          [field]: fieldValue,
        },
      },
    });
  };

  useEffect(() => {
    setLevels([
      { name: data?.garden?.name!, location: `/garden/${data?.garden?.id}` },
    ]);

    return () => setLevels([]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  if (loading) {
    return (
      <Typography variant="body1" gutterBottom={true} color="textPrimary">
        Loading
      </Typography>
    );
  }
  if (error) {
    return (
      <Typography variant="body1" gutterBottom={true} color="textPrimary">
        Error :( {JSON.stringify(error)}
      </Typography>
    );
  }

  const heightRange = range(
    data?.garden?.topSize || 0,
    data?.garden?.bottomSize || 0,
    1,
  );
  const widthRange = range(
    data?.garden?.leftSize || 0,
    data?.garden?.rightSize || 0,
    1,
  );

  return (
    <>
      <Typography variant="h4" color="textPrimary">
        {data?.garden?.name}
      </Typography>
      <Button color="primary" onClick={() => setDetailsModelOpen(true)}>
        Edit details
      </Button>
      <Typography variant="body1" color="textPrimary">
        {data?.garden?.description}
      </Typography>
      <div className={classes.gardenGridLayout}>
        <Button
          color="secondary"
          style={{ flex: '1 100%' }}
          onClick={() => increaseGardenSize('topSize')}
        >
          Add above
        </Button>
        <Button
          color="secondary"
          className={classes.sideButton}
          onClick={() => increaseGardenSize('leftSize')}
        >
          Add left
        </Button>
        <div className={classes.gardenGridContainer}>
          <div className={classes.gardenGrid}>
            {heightRange.map((height) => (
              <div key={height} className={classes.gardenRow}>
                {widthRange.map((width) => (
                  <GardenPixel
                    key={width}
                    {...data?.garden?.pixels.find(
                      (pixel) => pixel.y === height && pixel.x === width,
                    )}
                    y={height}
                    x={width}
                    onClick={() => {
                      const pixel = data?.garden?.pixels.find(
                        (pixel) => pixel.y === height && pixel.x === width,
                      );
                      setSelectedPixel({
                        x: width,
                        y: height,
                        id: pixel?.id,
                        colorString: pixel?.color,
                        plantable: pixel?.plantable,
                      });
                      setPixelModalOpen(true);
                    }}
                  />
                ))}
              </div>
            ))}
          </div>
        </div>

        <Button
          color="secondary"
          className={classes.sideButton}
          onClick={() => increaseGardenSize('rightSize')}
        >
          Add right
        </Button>
        <Button
          color="secondary"
          style={{ flex: '1 100%' }}
          onClick={() => increaseGardenSize('bottomSize')}
        >
          Add below
        </Button>
      </div>
      <Typography
        variant="body2"
        color="textPrimary"
        style={{ whiteSpace: 'pre-wrap' }}
      >
        <strong>Notes</strong>
        <br />
        {data?.garden?.notes}
      </Typography>
      {data?.garden && (
        <>
          <EditGardenDetailsModal
            open={detailsModelOpen}
            initialValues={data.garden}
            handleClose={() => setDetailsModelOpen(false)}
            handleCommit={saveDetails}
          />
          <EditGardenPixelModal
            open={pixelModalOpen}
            initialValues={selectedPixel}
            handleClose={() => setPixelModalOpen(false)}
            handleCommit={saveGardenPixel}
          />
        </>
      )}
    </>
  );
};

export default Garden;

/// <reference types="react-scripts" />

interface Window {
  env: { initialized: boolean; [key: string]: string };
}

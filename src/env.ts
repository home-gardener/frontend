const ENV = window.env.initialized ? window.env : process.env;

export default ENV;

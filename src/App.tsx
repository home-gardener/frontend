import { blue, green } from '@material-ui/core/colors';
import {
  createMuiTheme,
  darken,
  ThemeProvider,
} from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import * as Sentry from '@sentry/browser';
import React from 'react';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { Route, Router, Switch } from 'react-router-dom';
import Frame from './components/Frame';
import Garden from './components/Garden';
import Home from './components/Home';
import NavBar from './components/NavBar';
import PrivateRoute from './components/PrivateRoute';
import Profile from './components/Profile';
import { BreadCrumbProvider } from './context/BreadCrumbContext';
import { useAuth0 } from './react-auth0-spa';
import history from './utils/history';

function App() {
  const { isInitializing, isAuthenticated, user } = useAuth0();
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const paletteType = prefersDarkMode ? 'dark' : 'light';

  const theme = React.useMemo(() => {
    const newTheme = createMuiTheme({
      palette: {
        primary: {
          main: paletteType === 'light' ? blue[700] : blue[200],
        },
        secondary: {
          main: paletteType === 'light' ? darken(green.A400, 0.1) : green[200],
        },
        type: paletteType,
        background: {
          default: paletteType === 'light' ? '#fff' : '#121212',
        },
      },
      overrides: {
        MuiInputLabel: {
          // Name of the component ⚛️ / style sheet
          root: {
            // Name of the rule
            left: '4px',
          },
        },
      },
    });

    newTheme.palette.background.default =
      paletteType === 'light'
        ? newTheme.palette.grey[100]
        : newTheme.palette.grey[900];

    newTheme.palette.background.paper =
      paletteType === 'light' ? '#fff' : '#333';

    return newTheme;
  }, [paletteType]);

  if (isInitializing) {
    return <div>Loading...</div>;
  }

  if (isAuthenticated) {
    Sentry.configureScope((scope) => {
      scope.setUser({
        email: user.email,
        id: user.sub,
        username: user.email,
      });
    });
  }

  return (
    <HelmetProvider>
      <ThemeProvider theme={theme}>
        <Helmet>
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
          />
        </Helmet>
        <Router history={history}>
          <header>
            <NavBar />
          </header>
          <Switch>
            <BreadCrumbProvider>
              <Frame>
                <Route path="/" exact component={Home} />
                <PrivateRoute path="/profile" component={Profile} exact />
                <PrivateRoute path="/garden/:id" component={Garden} exact />
              </Frame>
            </BreadCrumbProvider>
          </Switch>
        </Router>
      </ThemeProvider>
    </HelmetProvider>
  );
}

export default App;

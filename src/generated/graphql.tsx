import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
};

export type BatchPayload = {
   __typename?: 'BatchPayload';
  count: Scalars['Int'];
};

export type BooleanFilter = {
  equals?: Maybe<Scalars['Boolean']>;
  not?: Maybe<Scalars['Boolean']>;
};


export type DateTimeFilter = {
  equals?: Maybe<Scalars['DateTime']>;
  gt?: Maybe<Scalars['DateTime']>;
  gte?: Maybe<Scalars['DateTime']>;
  in?: Maybe<Array<Scalars['DateTime']>>;
  lt?: Maybe<Scalars['DateTime']>;
  lte?: Maybe<Scalars['DateTime']>;
  not?: Maybe<Scalars['DateTime']>;
  notIn?: Maybe<Array<Scalars['DateTime']>>;
};

export type Garden = {
   __typename?: 'Garden';
  bottomSize: Scalars['Int'];
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  leftSize: Scalars['Int'];
  memberships: Array<GardenMember>;
  name: Scalars['String'];
  notes?: Maybe<Scalars['String']>;
  pixels: Array<GardenPixel>;
  rightSize: Scalars['Int'];
  topSize: Scalars['Int'];
  updatedAt: Scalars['DateTime'];
};


export type GardenMembershipsArgs = {
  after?: Maybe<GardenMemberWhereUniqueInput>;
  before?: Maybe<GardenMemberWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
};


export type GardenPixelsArgs = {
  after?: Maybe<GardenPixelWhereUniqueInput>;
  before?: Maybe<GardenPixelWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
};

export type GardenCreateInput = {
  bottomSize?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  leftSize?: Maybe<Scalars['Int']>;
  memberships?: Maybe<GardenMemberCreateManyWithoutGardenInput>;
  name: Scalars['String'];
  notes?: Maybe<Scalars['String']>;
  pixels?: Maybe<GardenPixelCreateManyWithoutGardenInput>;
  rightSize?: Maybe<Scalars['Int']>;
  topSize?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GardenCreateOneWithoutMembershipsInput = {
  connect?: Maybe<GardenWhereUniqueInput>;
  create?: Maybe<GardenCreateWithoutMembershipsInput>;
};

export type GardenCreateOneWithoutPixelsInput = {
  connect?: Maybe<GardenWhereUniqueInput>;
  create?: Maybe<GardenCreateWithoutPixelsInput>;
};

export type GardenCreateWithoutMembershipsInput = {
  bottomSize?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  leftSize?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  notes?: Maybe<Scalars['String']>;
  pixels?: Maybe<GardenPixelCreateManyWithoutGardenInput>;
  rightSize?: Maybe<Scalars['Int']>;
  topSize?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GardenCreateWithoutPixelsInput = {
  bottomSize?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  leftSize?: Maybe<Scalars['Int']>;
  memberships?: Maybe<GardenMemberCreateManyWithoutGardenInput>;
  name: Scalars['String'];
  notes?: Maybe<Scalars['String']>;
  rightSize?: Maybe<Scalars['Int']>;
  topSize?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GardenMember = {
   __typename?: 'GardenMember';
  garden: Garden;
  id: Scalars['String'];
  role: GardenRole;
  user: User;
};

export type GardenMemberCreateManyWithoutGardenInput = {
  connect?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  create?: Maybe<Array<GardenMemberCreateWithoutGardenInput>>;
};

export type GardenMemberCreateManyWithoutUserInput = {
  connect?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  create?: Maybe<Array<GardenMemberCreateWithoutUserInput>>;
};

export type GardenMemberCreateWithoutGardenInput = {
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  role: GardenRole;
  updatedAt?: Maybe<Scalars['DateTime']>;
  user: UserCreateOneWithoutMembershipsInput;
};

export type GardenMemberCreateWithoutUserInput = {
  createdAt?: Maybe<Scalars['DateTime']>;
  garden: GardenCreateOneWithoutMembershipsInput;
  id?: Maybe<Scalars['String']>;
  role: GardenRole;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GardenMemberFilter = {
  every?: Maybe<GardenMemberWhereInput>;
  none?: Maybe<GardenMemberWhereInput>;
  some?: Maybe<GardenMemberWhereInput>;
};

export type GardenMemberOrderByInput = {
  createdAt?: Maybe<OrderByArg>;
  gardenId?: Maybe<OrderByArg>;
  id?: Maybe<OrderByArg>;
  role?: Maybe<OrderByArg>;
  updatedAt?: Maybe<OrderByArg>;
  userId?: Maybe<OrderByArg>;
};

export type GardenMemberScalarWhereInput = {
  AND?: Maybe<Array<GardenMemberScalarWhereInput>>;
  createdAt?: Maybe<DateTimeFilter>;
  gardenId?: Maybe<StringFilter>;
  id?: Maybe<StringFilter>;
  NOT?: Maybe<Array<GardenMemberScalarWhereInput>>;
  OR?: Maybe<Array<GardenMemberScalarWhereInput>>;
  role?: Maybe<GardenRole>;
  updatedAt?: Maybe<DateTimeFilter>;
  userId?: Maybe<StringFilter>;
};

export type GardenMemberUpdateManyDataInput = {
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  role?: Maybe<GardenRole>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GardenMemberUpdateManyWithoutGardenInput = {
  connect?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  create?: Maybe<Array<GardenMemberCreateWithoutGardenInput>>;
  delete?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  deleteMany?: Maybe<Array<GardenMemberScalarWhereInput>>;
  disconnect?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  set?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  update?: Maybe<Array<GardenMemberUpdateWithWhereUniqueWithoutGardenInput>>;
  updateMany?: Maybe<Array<GardenMemberUpdateManyWithWhereNestedInput>>;
  upsert?: Maybe<Array<GardenMemberUpsertWithWhereUniqueWithoutGardenInput>>;
};

export type GardenMemberUpdateManyWithoutUserInput = {
  connect?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  create?: Maybe<Array<GardenMemberCreateWithoutUserInput>>;
  delete?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  deleteMany?: Maybe<Array<GardenMemberScalarWhereInput>>;
  disconnect?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  set?: Maybe<Array<GardenMemberWhereUniqueInput>>;
  update?: Maybe<Array<GardenMemberUpdateWithWhereUniqueWithoutUserInput>>;
  updateMany?: Maybe<Array<GardenMemberUpdateManyWithWhereNestedInput>>;
  upsert?: Maybe<Array<GardenMemberUpsertWithWhereUniqueWithoutUserInput>>;
};

export type GardenMemberUpdateManyWithWhereNestedInput = {
  data: GardenMemberUpdateManyDataInput;
  where: GardenMemberScalarWhereInput;
};

export type GardenMemberUpdateWithoutGardenDataInput = {
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  role?: Maybe<GardenRole>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  user?: Maybe<UserUpdateOneRequiredWithoutMembershipsInput>;
};

export type GardenMemberUpdateWithoutUserDataInput = {
  createdAt?: Maybe<Scalars['DateTime']>;
  garden?: Maybe<GardenUpdateOneRequiredWithoutMembershipsInput>;
  id?: Maybe<Scalars['String']>;
  role?: Maybe<GardenRole>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GardenMemberUpdateWithWhereUniqueWithoutGardenInput = {
  data: GardenMemberUpdateWithoutGardenDataInput;
  where: GardenMemberWhereUniqueInput;
};

export type GardenMemberUpdateWithWhereUniqueWithoutUserInput = {
  data: GardenMemberUpdateWithoutUserDataInput;
  where: GardenMemberWhereUniqueInput;
};

export type GardenMemberUpsertWithWhereUniqueWithoutGardenInput = {
  create: GardenMemberCreateWithoutGardenInput;
  update: GardenMemberUpdateWithoutGardenDataInput;
  where: GardenMemberWhereUniqueInput;
};

export type GardenMemberUpsertWithWhereUniqueWithoutUserInput = {
  create: GardenMemberCreateWithoutUserInput;
  update: GardenMemberUpdateWithoutUserDataInput;
  where: GardenMemberWhereUniqueInput;
};

export type GardenMemberWhereInput = {
  AND?: Maybe<Array<GardenMemberWhereInput>>;
  createdAt?: Maybe<DateTimeFilter>;
  garden?: Maybe<GardenWhereInput>;
  gardenId?: Maybe<StringFilter>;
  id?: Maybe<StringFilter>;
  NOT?: Maybe<Array<GardenMemberWhereInput>>;
  OR?: Maybe<Array<GardenMemberWhereInput>>;
  role?: Maybe<GardenRole>;
  updatedAt?: Maybe<DateTimeFilter>;
  user?: Maybe<UserWhereInput>;
  userId?: Maybe<StringFilter>;
};

export type GardenMemberWhereUniqueInput = {
  id?: Maybe<Scalars['String']>;
  userId_gardenId?: Maybe<UserIdGardenIdCompoundUniqueInput>;
};

export type GardenOrderByInput = {
  bottomSize?: Maybe<OrderByArg>;
  createdAt?: Maybe<OrderByArg>;
  description?: Maybe<OrderByArg>;
  id?: Maybe<OrderByArg>;
  leftSize?: Maybe<OrderByArg>;
  name?: Maybe<OrderByArg>;
  notes?: Maybe<OrderByArg>;
  rightSize?: Maybe<OrderByArg>;
  topSize?: Maybe<OrderByArg>;
  updatedAt?: Maybe<OrderByArg>;
};

export type GardenPixel = {
   __typename?: 'GardenPixel';
  color: Scalars['String'];
  garden: Garden;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id: Scalars['String'];
  notes?: Maybe<Scalars['String']>;
  plant?: Maybe<Plant>;
  plantable: Scalars['Boolean'];
  plantedBy?: Maybe<User>;
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type GardenPixelCreateInput = {
  color: Scalars['String'];
  garden: GardenCreateOneWithoutPixelsInput;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plant?: Maybe<PlantCreateOneWithoutGardenPixelsInput>;
  plantable: Scalars['Boolean'];
  plantedBy?: Maybe<UserCreateOneWithoutGardenPixelsInput>;
  plantedDate?: Maybe<Scalars['DateTime']>;
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type GardenPixelCreateManyWithoutGardenInput = {
  connect?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  create?: Maybe<Array<GardenPixelCreateWithoutGardenInput>>;
};

export type GardenPixelCreateManyWithoutPlantedByInput = {
  connect?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  create?: Maybe<Array<GardenPixelCreateWithoutPlantedByInput>>;
};

export type GardenPixelCreateManyWithoutPlantInput = {
  connect?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  create?: Maybe<Array<GardenPixelCreateWithoutPlantInput>>;
};

export type GardenPixelCreateWithoutGardenInput = {
  color: Scalars['String'];
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plant?: Maybe<PlantCreateOneWithoutGardenPixelsInput>;
  plantable: Scalars['Boolean'];
  plantedBy?: Maybe<UserCreateOneWithoutGardenPixelsInput>;
  plantedDate?: Maybe<Scalars['DateTime']>;
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type GardenPixelCreateWithoutPlantedByInput = {
  color: Scalars['String'];
  garden: GardenCreateOneWithoutPixelsInput;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plant?: Maybe<PlantCreateOneWithoutGardenPixelsInput>;
  plantable: Scalars['Boolean'];
  plantedDate?: Maybe<Scalars['DateTime']>;
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type GardenPixelCreateWithoutPlantInput = {
  color: Scalars['String'];
  garden: GardenCreateOneWithoutPixelsInput;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plantable: Scalars['Boolean'];
  plantedBy?: Maybe<UserCreateOneWithoutGardenPixelsInput>;
  plantedDate?: Maybe<Scalars['DateTime']>;
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type GardenPixelFilter = {
  every?: Maybe<GardenPixelWhereInput>;
  none?: Maybe<GardenPixelWhereInput>;
  some?: Maybe<GardenPixelWhereInput>;
};

export type GardenPixelOrderByInput = {
  color?: Maybe<OrderByArg>;
  gardenId?: Maybe<OrderByArg>;
  harvestedDate?: Maybe<OrderByArg>;
  id?: Maybe<OrderByArg>;
  notes?: Maybe<OrderByArg>;
  plantable?: Maybe<OrderByArg>;
  plantedByUserId?: Maybe<OrderByArg>;
  plantedDate?: Maybe<OrderByArg>;
  plantId?: Maybe<OrderByArg>;
  x?: Maybe<OrderByArg>;
  y?: Maybe<OrderByArg>;
};

export type GardenPixelScalarWhereInput = {
  AND?: Maybe<Array<GardenPixelScalarWhereInput>>;
  color?: Maybe<StringFilter>;
  gardenId?: Maybe<StringFilter>;
  harvestedDate?: Maybe<NullableDateTimeFilter>;
  id?: Maybe<StringFilter>;
  NOT?: Maybe<Array<GardenPixelScalarWhereInput>>;
  notes?: Maybe<NullableStringFilter>;
  OR?: Maybe<Array<GardenPixelScalarWhereInput>>;
  plantable?: Maybe<BooleanFilter>;
  plantedByUserId?: Maybe<NullableStringFilter>;
  plantedDate?: Maybe<NullableDateTimeFilter>;
  plantId?: Maybe<NullableStringFilter>;
  x?: Maybe<IntFilter>;
  y?: Maybe<IntFilter>;
};

export type GardenPixelUpdateInput = {
  color?: Maybe<Scalars['String']>;
  garden?: Maybe<GardenUpdateOneRequiredWithoutPixelsInput>;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plant?: Maybe<PlantUpdateOneWithoutGardenPixelsInput>;
  plantable?: Maybe<Scalars['Boolean']>;
  plantedBy?: Maybe<UserUpdateOneWithoutGardenPixelsInput>;
  plantedDate?: Maybe<Scalars['DateTime']>;
  x?: Maybe<Scalars['Int']>;
  y?: Maybe<Scalars['Int']>;
};

export type GardenPixelUpdateManyDataInput = {
  color?: Maybe<Scalars['String']>;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plantable?: Maybe<Scalars['Boolean']>;
  plantedDate?: Maybe<Scalars['DateTime']>;
  x?: Maybe<Scalars['Int']>;
  y?: Maybe<Scalars['Int']>;
};

export type GardenPixelUpdateManyMutationInput = {
  color?: Maybe<Scalars['String']>;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plantable?: Maybe<Scalars['Boolean']>;
  plantedDate?: Maybe<Scalars['DateTime']>;
  x?: Maybe<Scalars['Int']>;
  y?: Maybe<Scalars['Int']>;
};

export type GardenPixelUpdateManyWithoutGardenInput = {
  connect?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  create?: Maybe<Array<GardenPixelCreateWithoutGardenInput>>;
  delete?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  deleteMany?: Maybe<Array<GardenPixelScalarWhereInput>>;
  disconnect?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  set?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  update?: Maybe<Array<GardenPixelUpdateWithWhereUniqueWithoutGardenInput>>;
  updateMany?: Maybe<Array<GardenPixelUpdateManyWithWhereNestedInput>>;
  upsert?: Maybe<Array<GardenPixelUpsertWithWhereUniqueWithoutGardenInput>>;
};

export type GardenPixelUpdateManyWithoutPlantedByInput = {
  connect?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  create?: Maybe<Array<GardenPixelCreateWithoutPlantedByInput>>;
  delete?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  deleteMany?: Maybe<Array<GardenPixelScalarWhereInput>>;
  disconnect?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  set?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  update?: Maybe<Array<GardenPixelUpdateWithWhereUniqueWithoutPlantedByInput>>;
  updateMany?: Maybe<Array<GardenPixelUpdateManyWithWhereNestedInput>>;
  upsert?: Maybe<Array<GardenPixelUpsertWithWhereUniqueWithoutPlantedByInput>>;
};

export type GardenPixelUpdateManyWithoutPlantInput = {
  connect?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  create?: Maybe<Array<GardenPixelCreateWithoutPlantInput>>;
  delete?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  deleteMany?: Maybe<Array<GardenPixelScalarWhereInput>>;
  disconnect?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  set?: Maybe<Array<GardenPixelWhereUniqueInput>>;
  update?: Maybe<Array<GardenPixelUpdateWithWhereUniqueWithoutPlantInput>>;
  updateMany?: Maybe<Array<GardenPixelUpdateManyWithWhereNestedInput>>;
  upsert?: Maybe<Array<GardenPixelUpsertWithWhereUniqueWithoutPlantInput>>;
};

export type GardenPixelUpdateManyWithWhereNestedInput = {
  data: GardenPixelUpdateManyDataInput;
  where: GardenPixelScalarWhereInput;
};

export type GardenPixelUpdateWithoutGardenDataInput = {
  color?: Maybe<Scalars['String']>;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plant?: Maybe<PlantUpdateOneWithoutGardenPixelsInput>;
  plantable?: Maybe<Scalars['Boolean']>;
  plantedBy?: Maybe<UserUpdateOneWithoutGardenPixelsInput>;
  plantedDate?: Maybe<Scalars['DateTime']>;
  x?: Maybe<Scalars['Int']>;
  y?: Maybe<Scalars['Int']>;
};

export type GardenPixelUpdateWithoutPlantDataInput = {
  color?: Maybe<Scalars['String']>;
  garden?: Maybe<GardenUpdateOneRequiredWithoutPixelsInput>;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plantable?: Maybe<Scalars['Boolean']>;
  plantedBy?: Maybe<UserUpdateOneWithoutGardenPixelsInput>;
  plantedDate?: Maybe<Scalars['DateTime']>;
  x?: Maybe<Scalars['Int']>;
  y?: Maybe<Scalars['Int']>;
};

export type GardenPixelUpdateWithoutPlantedByDataInput = {
  color?: Maybe<Scalars['String']>;
  garden?: Maybe<GardenUpdateOneRequiredWithoutPixelsInput>;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  plant?: Maybe<PlantUpdateOneWithoutGardenPixelsInput>;
  plantable?: Maybe<Scalars['Boolean']>;
  plantedDate?: Maybe<Scalars['DateTime']>;
  x?: Maybe<Scalars['Int']>;
  y?: Maybe<Scalars['Int']>;
};

export type GardenPixelUpdateWithWhereUniqueWithoutGardenInput = {
  data: GardenPixelUpdateWithoutGardenDataInput;
  where: GardenPixelWhereUniqueInput;
};

export type GardenPixelUpdateWithWhereUniqueWithoutPlantedByInput = {
  data: GardenPixelUpdateWithoutPlantedByDataInput;
  where: GardenPixelWhereUniqueInput;
};

export type GardenPixelUpdateWithWhereUniqueWithoutPlantInput = {
  data: GardenPixelUpdateWithoutPlantDataInput;
  where: GardenPixelWhereUniqueInput;
};

export type GardenPixelUpsertWithWhereUniqueWithoutGardenInput = {
  create: GardenPixelCreateWithoutGardenInput;
  update: GardenPixelUpdateWithoutGardenDataInput;
  where: GardenPixelWhereUniqueInput;
};

export type GardenPixelUpsertWithWhereUniqueWithoutPlantedByInput = {
  create: GardenPixelCreateWithoutPlantedByInput;
  update: GardenPixelUpdateWithoutPlantedByDataInput;
  where: GardenPixelWhereUniqueInput;
};

export type GardenPixelUpsertWithWhereUniqueWithoutPlantInput = {
  create: GardenPixelCreateWithoutPlantInput;
  update: GardenPixelUpdateWithoutPlantDataInput;
  where: GardenPixelWhereUniqueInput;
};

export type GardenPixelWhereInput = {
  AND?: Maybe<Array<GardenPixelWhereInput>>;
  color?: Maybe<StringFilter>;
  garden?: Maybe<GardenWhereInput>;
  gardenId?: Maybe<StringFilter>;
  harvestedDate?: Maybe<NullableDateTimeFilter>;
  id?: Maybe<StringFilter>;
  NOT?: Maybe<Array<GardenPixelWhereInput>>;
  notes?: Maybe<NullableStringFilter>;
  OR?: Maybe<Array<GardenPixelWhereInput>>;
  plant?: Maybe<PlantWhereInput>;
  plantable?: Maybe<BooleanFilter>;
  plantedBy?: Maybe<UserWhereInput>;
  plantedByUserId?: Maybe<NullableStringFilter>;
  plantedDate?: Maybe<NullableDateTimeFilter>;
  plantId?: Maybe<NullableStringFilter>;
  x?: Maybe<IntFilter>;
  y?: Maybe<IntFilter>;
};

export type GardenPixelWhereUniqueInput = {
  id?: Maybe<Scalars['String']>;
};

export enum GardenRole {
  Admin = 'ADMIN',
  Editor = 'EDITOR',
  Viewer = 'VIEWER'
}

export type GardenUpdateInput = {
  bottomSize?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  leftSize?: Maybe<Scalars['Int']>;
  memberships?: Maybe<GardenMemberUpdateManyWithoutGardenInput>;
  name?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  pixels?: Maybe<GardenPixelUpdateManyWithoutGardenInput>;
  rightSize?: Maybe<Scalars['Int']>;
  topSize?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GardenUpdateOneRequiredWithoutMembershipsInput = {
  connect?: Maybe<GardenWhereUniqueInput>;
  create?: Maybe<GardenCreateWithoutMembershipsInput>;
  update?: Maybe<GardenUpdateWithoutMembershipsDataInput>;
  upsert?: Maybe<GardenUpsertWithoutMembershipsInput>;
};

export type GardenUpdateOneRequiredWithoutPixelsInput = {
  connect?: Maybe<GardenWhereUniqueInput>;
  create?: Maybe<GardenCreateWithoutPixelsInput>;
  update?: Maybe<GardenUpdateWithoutPixelsDataInput>;
  upsert?: Maybe<GardenUpsertWithoutPixelsInput>;
};

export type GardenUpdateWithoutMembershipsDataInput = {
  bottomSize?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  leftSize?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  pixels?: Maybe<GardenPixelUpdateManyWithoutGardenInput>;
  rightSize?: Maybe<Scalars['Int']>;
  topSize?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GardenUpdateWithoutPixelsDataInput = {
  bottomSize?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  leftSize?: Maybe<Scalars['Int']>;
  memberships?: Maybe<GardenMemberUpdateManyWithoutGardenInput>;
  name?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  rightSize?: Maybe<Scalars['Int']>;
  topSize?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type GardenUpsertWithoutMembershipsInput = {
  create: GardenCreateWithoutMembershipsInput;
  update: GardenUpdateWithoutMembershipsDataInput;
};

export type GardenUpsertWithoutPixelsInput = {
  create: GardenCreateWithoutPixelsInput;
  update: GardenUpdateWithoutPixelsDataInput;
};

export type GardenWhereInput = {
  AND?: Maybe<Array<GardenWhereInput>>;
  bottomSize?: Maybe<IntFilter>;
  createdAt?: Maybe<DateTimeFilter>;
  description?: Maybe<NullableStringFilter>;
  id?: Maybe<StringFilter>;
  leftSize?: Maybe<IntFilter>;
  memberships?: Maybe<GardenMemberFilter>;
  name?: Maybe<StringFilter>;
  NOT?: Maybe<Array<GardenWhereInput>>;
  notes?: Maybe<NullableStringFilter>;
  OR?: Maybe<Array<GardenWhereInput>>;
  pixels?: Maybe<GardenPixelFilter>;
  rightSize?: Maybe<IntFilter>;
  topSize?: Maybe<IntFilter>;
  updatedAt?: Maybe<DateTimeFilter>;
};

export type GardenWhereUniqueInput = {
  id?: Maybe<Scalars['String']>;
};

export type IntFilter = {
  equals?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  gte?: Maybe<Scalars['Int']>;
  in?: Maybe<Array<Scalars['Int']>>;
  lt?: Maybe<Scalars['Int']>;
  lte?: Maybe<Scalars['Int']>;
  not?: Maybe<Scalars['Int']>;
  notIn?: Maybe<Array<Scalars['Int']>>;
};

export type JwtSubCreateManyWithoutUserInput = {
  connect?: Maybe<Array<JwtSubWhereUniqueInput>>;
  create?: Maybe<Array<JwtSubCreateWithoutUserInput>>;
};

export type JwtSubCreateWithoutUserInput = {
  id?: Maybe<Scalars['String']>;
  sub: Scalars['String'];
};

export type JwtSubFilter = {
  every?: Maybe<JwtSubWhereInput>;
  none?: Maybe<JwtSubWhereInput>;
  some?: Maybe<JwtSubWhereInput>;
};

export type JwtSubScalarWhereInput = {
  AND?: Maybe<Array<JwtSubScalarWhereInput>>;
  id?: Maybe<StringFilter>;
  NOT?: Maybe<Array<JwtSubScalarWhereInput>>;
  OR?: Maybe<Array<JwtSubScalarWhereInput>>;
  sub?: Maybe<StringFilter>;
  userId?: Maybe<StringFilter>;
};

export type JwtSubUpdateManyDataInput = {
  id?: Maybe<Scalars['String']>;
  sub?: Maybe<Scalars['String']>;
};

export type JwtSubUpdateManyWithoutUserInput = {
  connect?: Maybe<Array<JwtSubWhereUniqueInput>>;
  create?: Maybe<Array<JwtSubCreateWithoutUserInput>>;
  delete?: Maybe<Array<JwtSubWhereUniqueInput>>;
  deleteMany?: Maybe<Array<JwtSubScalarWhereInput>>;
  disconnect?: Maybe<Array<JwtSubWhereUniqueInput>>;
  set?: Maybe<Array<JwtSubWhereUniqueInput>>;
  update?: Maybe<Array<JwtSubUpdateWithWhereUniqueWithoutUserInput>>;
  updateMany?: Maybe<Array<JwtSubUpdateManyWithWhereNestedInput>>;
  upsert?: Maybe<Array<JwtSubUpsertWithWhereUniqueWithoutUserInput>>;
};

export type JwtSubUpdateManyWithWhereNestedInput = {
  data: JwtSubUpdateManyDataInput;
  where: JwtSubScalarWhereInput;
};

export type JwtSubUpdateWithoutUserDataInput = {
  id?: Maybe<Scalars['String']>;
  sub?: Maybe<Scalars['String']>;
};

export type JwtSubUpdateWithWhereUniqueWithoutUserInput = {
  data: JwtSubUpdateWithoutUserDataInput;
  where: JwtSubWhereUniqueInput;
};

export type JwtSubUpsertWithWhereUniqueWithoutUserInput = {
  create: JwtSubCreateWithoutUserInput;
  update: JwtSubUpdateWithoutUserDataInput;
  where: JwtSubWhereUniqueInput;
};

export type JwtSubWhereInput = {
  AND?: Maybe<Array<JwtSubWhereInput>>;
  id?: Maybe<StringFilter>;
  NOT?: Maybe<Array<JwtSubWhereInput>>;
  OR?: Maybe<Array<JwtSubWhereInput>>;
  sub?: Maybe<StringFilter>;
  user?: Maybe<UserWhereInput>;
  userId?: Maybe<StringFilter>;
};

export type JwtSubWhereUniqueInput = {
  id?: Maybe<Scalars['String']>;
  sub?: Maybe<Scalars['String']>;
};

export type Mutation = {
   __typename?: 'Mutation';
  createOneGarden: Garden;
  createOneGardenPixel: GardenPixel;
  createOnePlant: Plant;
  deleteOneGarden?: Maybe<Garden>;
  deleteOneGardenMember?: Maybe<GardenMember>;
  deleteOneGardenPixel?: Maybe<GardenPixel>;
  deleteOnePlant?: Maybe<Plant>;
  setGardenMember: Scalars['Boolean'];
  updateManyGardenPixel: BatchPayload;
  updateOneGarden?: Maybe<Garden>;
  updateOneGardenPixel?: Maybe<GardenPixel>;
  updateOnePlant?: Maybe<Plant>;
  upsertOneGardenPixel: GardenPixel;
};


export type MutationCreateOneGardenArgs = {
  data: GardenCreateInput;
};


export type MutationCreateOneGardenPixelArgs = {
  data: GardenPixelCreateInput;
};


export type MutationCreateOnePlantArgs = {
  data: PlantCreateInput;
};


export type MutationDeleteOneGardenArgs = {
  where: GardenWhereUniqueInput;
};


export type MutationDeleteOneGardenMemberArgs = {
  where: GardenMemberWhereUniqueInput;
};


export type MutationDeleteOneGardenPixelArgs = {
  where: GardenPixelWhereUniqueInput;
};


export type MutationDeleteOnePlantArgs = {
  where: PlantWhereUniqueInput;
};


export type MutationSetGardenMemberArgs = {
  email?: Maybe<Scalars['String']>;
  gardenId: Scalars['String'];
  id?: Maybe<Scalars['String']>;
  role: Scalars['String'];
};


export type MutationUpdateManyGardenPixelArgs = {
  data: GardenPixelUpdateManyMutationInput;
  where?: Maybe<GardenPixelWhereInput>;
};


export type MutationUpdateOneGardenArgs = {
  data: GardenUpdateInput;
  where: GardenWhereUniqueInput;
};


export type MutationUpdateOneGardenPixelArgs = {
  data: GardenPixelUpdateInput;
  where: GardenPixelWhereUniqueInput;
};


export type MutationUpdateOnePlantArgs = {
  data: PlantUpdateInput;
  where: PlantWhereUniqueInput;
};


export type MutationUpsertOneGardenPixelArgs = {
  create: GardenPixelCreateInput;
  update: GardenPixelUpdateInput;
  where: GardenPixelWhereUniqueInput;
};

export type NullableDateTimeFilter = {
  equals?: Maybe<Scalars['DateTime']>;
  gt?: Maybe<Scalars['DateTime']>;
  gte?: Maybe<Scalars['DateTime']>;
  in?: Maybe<Array<Scalars['DateTime']>>;
  lt?: Maybe<Scalars['DateTime']>;
  lte?: Maybe<Scalars['DateTime']>;
  not?: Maybe<Scalars['DateTime']>;
  notIn?: Maybe<Array<Scalars['DateTime']>>;
};

export type NullableStringFilter = {
  contains?: Maybe<Scalars['String']>;
  endsWith?: Maybe<Scalars['String']>;
  equals?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  not?: Maybe<Scalars['String']>;
  notIn?: Maybe<Array<Scalars['String']>>;
  startsWith?: Maybe<Scalars['String']>;
};

export enum OrderByArg {
  Asc = 'asc',
  Desc = 'desc'
}

export type Plant = {
   __typename?: 'Plant';
  daysToHarvest: Scalars['Int'];
  gardenPixels: Array<GardenPixel>;
  id: Scalars['String'];
  name: Scalars['String'];
};


export type PlantGardenPixelsArgs = {
  after?: Maybe<GardenPixelWhereUniqueInput>;
  before?: Maybe<GardenPixelWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
};

export type PlantCreateInput = {
  daysToHarvest: Scalars['Int'];
  gardenPixels?: Maybe<GardenPixelCreateManyWithoutPlantInput>;
  id?: Maybe<Scalars['String']>;
  name: Scalars['String'];
};

export type PlantCreateOneWithoutGardenPixelsInput = {
  connect?: Maybe<PlantWhereUniqueInput>;
  create?: Maybe<PlantCreateWithoutGardenPixelsInput>;
};

export type PlantCreateWithoutGardenPixelsInput = {
  daysToHarvest: Scalars['Int'];
  id?: Maybe<Scalars['String']>;
  name: Scalars['String'];
};

export type PlantOrderByInput = {
  daysToHarvest?: Maybe<OrderByArg>;
  id?: Maybe<OrderByArg>;
  name?: Maybe<OrderByArg>;
};

export type PlantUpdateInput = {
  daysToHarvest?: Maybe<Scalars['Int']>;
  gardenPixels?: Maybe<GardenPixelUpdateManyWithoutPlantInput>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

export type PlantUpdateOneWithoutGardenPixelsInput = {
  connect?: Maybe<PlantWhereUniqueInput>;
  create?: Maybe<PlantCreateWithoutGardenPixelsInput>;
  delete?: Maybe<Scalars['Boolean']>;
  disconnect?: Maybe<Scalars['Boolean']>;
  update?: Maybe<PlantUpdateWithoutGardenPixelsDataInput>;
  upsert?: Maybe<PlantUpsertWithoutGardenPixelsInput>;
};

export type PlantUpdateWithoutGardenPixelsDataInput = {
  daysToHarvest?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

export type PlantUpsertWithoutGardenPixelsInput = {
  create: PlantCreateWithoutGardenPixelsInput;
  update: PlantUpdateWithoutGardenPixelsDataInput;
};

export type PlantWhereInput = {
  AND?: Maybe<Array<PlantWhereInput>>;
  daysToHarvest?: Maybe<IntFilter>;
  gardenPixels?: Maybe<GardenPixelFilter>;
  id?: Maybe<StringFilter>;
  name?: Maybe<StringFilter>;
  NOT?: Maybe<Array<PlantWhereInput>>;
  OR?: Maybe<Array<PlantWhereInput>>;
};

export type PlantWhereUniqueInput = {
  id?: Maybe<Scalars['String']>;
};

export type Query = {
   __typename?: 'Query';
  garden?: Maybe<Garden>;
  gardenMember?: Maybe<GardenMember>;
  gardenMembers: Array<GardenMember>;
  gardenPixel?: Maybe<GardenPixel>;
  gardenPixels: Array<GardenPixel>;
  gardens: Array<Garden>;
  me?: Maybe<User>;
  plant?: Maybe<Plant>;
  plants: Array<Plant>;
  user?: Maybe<User>;
  users: Array<User>;
};


export type QueryGardenArgs = {
  where: GardenWhereUniqueInput;
};


export type QueryGardenMemberArgs = {
  where: GardenMemberWhereUniqueInput;
};


export type QueryGardenMembersArgs = {
  after?: Maybe<GardenMemberWhereUniqueInput>;
  before?: Maybe<GardenMemberWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<GardenMemberOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  where?: Maybe<GardenMemberWhereInput>;
};


export type QueryGardenPixelArgs = {
  where: GardenPixelWhereUniqueInput;
};


export type QueryGardenPixelsArgs = {
  after?: Maybe<GardenPixelWhereUniqueInput>;
  before?: Maybe<GardenPixelWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<GardenPixelOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  where?: Maybe<GardenPixelWhereInput>;
};


export type QueryGardensArgs = {
  after?: Maybe<GardenWhereUniqueInput>;
  before?: Maybe<GardenWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<GardenOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  where?: Maybe<GardenWhereInput>;
};


export type QueryPlantArgs = {
  where: PlantWhereUniqueInput;
};


export type QueryPlantsArgs = {
  after?: Maybe<PlantWhereUniqueInput>;
  before?: Maybe<PlantWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<PlantOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
};


export type QueryUserArgs = {
  where: UserWhereUniqueInput;
};


export type QueryUsersArgs = {
  after?: Maybe<UserWhereUniqueInput>;
  before?: Maybe<UserWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
};

export type StringFilter = {
  contains?: Maybe<Scalars['String']>;
  endsWith?: Maybe<Scalars['String']>;
  equals?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  not?: Maybe<Scalars['String']>;
  notIn?: Maybe<Array<Scalars['String']>>;
  startsWith?: Maybe<Scalars['String']>;
};

export type User = {
   __typename?: 'User';
  email: Scalars['String'];
  gardenPixels: Array<GardenPixel>;
  id: Scalars['String'];
  memberships: Array<GardenMember>;
  name: Scalars['String'];
};


export type UserGardenPixelsArgs = {
  after?: Maybe<GardenPixelWhereUniqueInput>;
  before?: Maybe<GardenPixelWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
};


export type UserMembershipsArgs = {
  after?: Maybe<GardenMemberWhereUniqueInput>;
  before?: Maybe<GardenMemberWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
};

export type UserCreateOneWithoutGardenPixelsInput = {
  connect?: Maybe<UserWhereUniqueInput>;
  create?: Maybe<UserCreateWithoutGardenPixelsInput>;
};

export type UserCreateOneWithoutMembershipsInput = {
  connect?: Maybe<UserWhereUniqueInput>;
  create?: Maybe<UserCreateWithoutMembershipsInput>;
};

export type UserCreateWithoutGardenPixelsInput = {
  createdAt?: Maybe<Scalars['DateTime']>;
  email: Scalars['String'];
  id?: Maybe<Scalars['String']>;
  jwtSub?: Maybe<JwtSubCreateManyWithoutUserInput>;
  memberships?: Maybe<GardenMemberCreateManyWithoutUserInput>;
  name: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type UserCreateWithoutMembershipsInput = {
  createdAt?: Maybe<Scalars['DateTime']>;
  email: Scalars['String'];
  gardenPixels?: Maybe<GardenPixelCreateManyWithoutPlantedByInput>;
  id?: Maybe<Scalars['String']>;
  jwtSub?: Maybe<JwtSubCreateManyWithoutUserInput>;
  name: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type UserIdGardenIdCompoundUniqueInput = {
  gardenId: Scalars['String'];
  userId: Scalars['String'];
};

export type UserOrderByInput = {
  createdAt?: Maybe<OrderByArg>;
  email?: Maybe<OrderByArg>;
  id?: Maybe<OrderByArg>;
  name?: Maybe<OrderByArg>;
  updatedAt?: Maybe<OrderByArg>;
};

export type UserUpdateOneRequiredWithoutMembershipsInput = {
  connect?: Maybe<UserWhereUniqueInput>;
  create?: Maybe<UserCreateWithoutMembershipsInput>;
  update?: Maybe<UserUpdateWithoutMembershipsDataInput>;
  upsert?: Maybe<UserUpsertWithoutMembershipsInput>;
};

export type UserUpdateOneWithoutGardenPixelsInput = {
  connect?: Maybe<UserWhereUniqueInput>;
  create?: Maybe<UserCreateWithoutGardenPixelsInput>;
  delete?: Maybe<Scalars['Boolean']>;
  disconnect?: Maybe<Scalars['Boolean']>;
  update?: Maybe<UserUpdateWithoutGardenPixelsDataInput>;
  upsert?: Maybe<UserUpsertWithoutGardenPixelsInput>;
};

export type UserUpdateWithoutGardenPixelsDataInput = {
  createdAt?: Maybe<Scalars['DateTime']>;
  email?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  jwtSub?: Maybe<JwtSubUpdateManyWithoutUserInput>;
  memberships?: Maybe<GardenMemberUpdateManyWithoutUserInput>;
  name?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type UserUpdateWithoutMembershipsDataInput = {
  createdAt?: Maybe<Scalars['DateTime']>;
  email?: Maybe<Scalars['String']>;
  gardenPixels?: Maybe<GardenPixelUpdateManyWithoutPlantedByInput>;
  id?: Maybe<Scalars['String']>;
  jwtSub?: Maybe<JwtSubUpdateManyWithoutUserInput>;
  name?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type UserUpsertWithoutGardenPixelsInput = {
  create: UserCreateWithoutGardenPixelsInput;
  update: UserUpdateWithoutGardenPixelsDataInput;
};

export type UserUpsertWithoutMembershipsInput = {
  create: UserCreateWithoutMembershipsInput;
  update: UserUpdateWithoutMembershipsDataInput;
};

export type UserWhereInput = {
  AND?: Maybe<Array<UserWhereInput>>;
  createdAt?: Maybe<DateTimeFilter>;
  email?: Maybe<StringFilter>;
  gardenPixels?: Maybe<GardenPixelFilter>;
  id?: Maybe<StringFilter>;
  jwtSub?: Maybe<JwtSubFilter>;
  memberships?: Maybe<GardenMemberFilter>;
  name?: Maybe<StringFilter>;
  NOT?: Maybe<Array<UserWhereInput>>;
  OR?: Maybe<Array<UserWhereInput>>;
  updatedAt?: Maybe<DateTimeFilter>;
};

export type UserWhereUniqueInput = {
  email?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
};

export type CreateOneGardenMutationVariables = {
  name: Scalars['String'];
  memberships?: Maybe<GardenMemberCreateManyWithoutGardenInput>;
};


export type CreateOneGardenMutation = (
  { __typename?: 'Mutation' }
  & { createOneGarden: (
    { __typename?: 'Garden' }
    & Pick<Garden, 'id' | 'name' | 'description'>
  ) }
);

export type GardenQueryVariables = {
  id: Scalars['String'];
};


export type GardenQuery = (
  { __typename?: 'Query' }
  & { garden?: Maybe<(
    { __typename?: 'Garden' }
    & { memberships: Array<(
      { __typename?: 'GardenMember' }
      & { user: (
        { __typename?: 'User' }
        & Pick<User, 'id' | 'name'>
      ) }
    )>, pixels: Array<(
      { __typename?: 'GardenPixel' }
      & GardenPixelFieldsFragment
    )> }
    & GardenFieldsFragment
  )> }
);

export type GardenFieldsFragment = (
  { __typename?: 'Garden' }
  & Pick<Garden, 'id' | 'name' | 'description' | 'notes' | 'topSize' | 'bottomSize' | 'leftSize' | 'rightSize'>
);

export type GardenPixelFieldsFragment = (
  { __typename?: 'GardenPixel' }
  & Pick<GardenPixel, 'id' | 'x' | 'y' | 'color' | 'plantable' | 'harvestedDate'>
  & { plant?: Maybe<(
    { __typename?: 'Plant' }
    & Pick<Plant, 'id' | 'name' | 'daysToHarvest'>
  )>, plantedBy?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id' | 'name'>
  )> }
);

export type MeQueryVariables = {};


export type MeQuery = (
  { __typename?: 'Query' }
  & { me?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id'>
  )> }
);

export type MyGardensQueryVariables = {
  userId: Scalars['String'];
};


export type MyGardensQuery = (
  { __typename?: 'Query' }
  & { gardens: Array<(
    { __typename?: 'Garden' }
    & Pick<Garden, 'id' | 'name' | 'description'>
  )> }
);

export type UpdateOneGardenMutationVariables = {
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  topSize?: Maybe<Scalars['Int']>;
  bottomSize?: Maybe<Scalars['Int']>;
  leftSize?: Maybe<Scalars['Int']>;
  rightSize?: Maybe<Scalars['Int']>;
  notes?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
};


export type UpdateOneGardenMutation = (
  { __typename?: 'Mutation' }
  & { updateOneGarden?: Maybe<(
    { __typename?: 'Garden' }
    & GardenFieldsFragment
  )> }
);

export type UpsertOneGardenPixelMutationVariables = {
  id?: Maybe<Scalars['String']>;
  color: Scalars['String'];
  plantCreate?: Maybe<PlantCreateOneWithoutGardenPixelsInput>;
  plantUpdate?: Maybe<PlantUpdateOneWithoutGardenPixelsInput>;
  plantable: Scalars['Boolean'];
  plantedDate?: Maybe<Scalars['DateTime']>;
  gardenId: Scalars['String'];
  plantedByCreate?: Maybe<UserCreateOneWithoutGardenPixelsInput>;
  plantedByUpdate?: Maybe<UserUpdateOneWithoutGardenPixelsInput>;
  harvestedDate?: Maybe<Scalars['DateTime']>;
  notes?: Maybe<Scalars['String']>;
  x: Scalars['Int'];
  y: Scalars['Int'];
};


export type UpsertOneGardenPixelMutation = (
  { __typename?: 'Mutation' }
  & { upsertOneGardenPixel: (
    { __typename?: 'GardenPixel' }
    & GardenPixelFieldsFragment
  ) }
);

export const GardenFieldsFragmentDoc = gql`
    fragment GardenFields on Garden {
  id
  name
  description
  notes
  topSize
  bottomSize
  leftSize
  rightSize
}
    `;
export const GardenPixelFieldsFragmentDoc = gql`
    fragment GardenPixelFields on GardenPixel {
  id
  x
  y
  color
  plant {
    id
    name
    daysToHarvest
  }
  plantable
  plantedBy {
    id
    name
  }
  harvestedDate
}
    `;
export const CreateOneGardenDocument = gql`
    mutation createOneGarden($name: String!, $memberships: GardenMemberCreateManyWithoutGardenInput) {
  createOneGarden(data: {name: $name, memberships: $memberships}) {
    id
    name
    description
  }
}
    `;
export type CreateOneGardenMutationFn = ApolloReactCommon.MutationFunction<CreateOneGardenMutation, CreateOneGardenMutationVariables>;
export type CreateOneGardenComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateOneGardenMutation, CreateOneGardenMutationVariables>, 'mutation'>;

    export const CreateOneGardenComponent = (props: CreateOneGardenComponentProps) => (
      <ApolloReactComponents.Mutation<CreateOneGardenMutation, CreateOneGardenMutationVariables> mutation={CreateOneGardenDocument} {...props} />
    );
    

/**
 * __useCreateOneGardenMutation__
 *
 * To run a mutation, you first call `useCreateOneGardenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateOneGardenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createOneGardenMutation, { data, loading, error }] = useCreateOneGardenMutation({
 *   variables: {
 *      name: // value for 'name'
 *      memberships: // value for 'memberships'
 *   },
 * });
 */
export function useCreateOneGardenMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateOneGardenMutation, CreateOneGardenMutationVariables>) {
        return ApolloReactHooks.useMutation<CreateOneGardenMutation, CreateOneGardenMutationVariables>(CreateOneGardenDocument, baseOptions);
      }
export type CreateOneGardenMutationHookResult = ReturnType<typeof useCreateOneGardenMutation>;
export type CreateOneGardenMutationResult = ApolloReactCommon.MutationResult<CreateOneGardenMutation>;
export type CreateOneGardenMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateOneGardenMutation, CreateOneGardenMutationVariables>;
export const GardenDocument = gql`
    query garden($id: String!) {
  garden(where: {id: $id}) {
    ...GardenFields
    memberships {
      user {
        id
        name
      }
    }
    pixels {
      ...GardenPixelFields
    }
  }
}
    ${GardenFieldsFragmentDoc}
${GardenPixelFieldsFragmentDoc}`;
export type GardenComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GardenQuery, GardenQueryVariables>, 'query'> & ({ variables: GardenQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GardenComponent = (props: GardenComponentProps) => (
      <ApolloReactComponents.Query<GardenQuery, GardenQueryVariables> query={GardenDocument} {...props} />
    );
    

/**
 * __useGardenQuery__
 *
 * To run a query within a React component, call `useGardenQuery` and pass it any options that fit your needs.
 * When your component renders, `useGardenQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGardenQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGardenQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GardenQuery, GardenQueryVariables>) {
        return ApolloReactHooks.useQuery<GardenQuery, GardenQueryVariables>(GardenDocument, baseOptions);
      }
export function useGardenLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GardenQuery, GardenQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GardenQuery, GardenQueryVariables>(GardenDocument, baseOptions);
        }
export type GardenQueryHookResult = ReturnType<typeof useGardenQuery>;
export type GardenLazyQueryHookResult = ReturnType<typeof useGardenLazyQuery>;
export type GardenQueryResult = ApolloReactCommon.QueryResult<GardenQuery, GardenQueryVariables>;
export const MeDocument = gql`
    query me {
  me {
    id
  }
}
    `;
export type MeComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<MeQuery, MeQueryVariables>, 'query'>;

    export const MeComponent = (props: MeComponentProps) => (
      <ApolloReactComponents.Query<MeQuery, MeQueryVariables> query={MeDocument} {...props} />
    );
    

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<MeQuery, MeQueryVariables>) {
        return ApolloReactHooks.useQuery<MeQuery, MeQueryVariables>(MeDocument, baseOptions);
      }
export function useMeLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, baseOptions);
        }
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = ApolloReactCommon.QueryResult<MeQuery, MeQueryVariables>;
export const MyGardensDocument = gql`
    query myGardens($userId: String!) {
  gardens(where: {memberships: {some: {userId: {equals: $userId}}}}) {
    id
    name
    description
  }
}
    `;
export type MyGardensComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<MyGardensQuery, MyGardensQueryVariables>, 'query'> & ({ variables: MyGardensQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const MyGardensComponent = (props: MyGardensComponentProps) => (
      <ApolloReactComponents.Query<MyGardensQuery, MyGardensQueryVariables> query={MyGardensDocument} {...props} />
    );
    

/**
 * __useMyGardensQuery__
 *
 * To run a query within a React component, call `useMyGardensQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyGardensQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyGardensQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useMyGardensQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<MyGardensQuery, MyGardensQueryVariables>) {
        return ApolloReactHooks.useQuery<MyGardensQuery, MyGardensQueryVariables>(MyGardensDocument, baseOptions);
      }
export function useMyGardensLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<MyGardensQuery, MyGardensQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<MyGardensQuery, MyGardensQueryVariables>(MyGardensDocument, baseOptions);
        }
export type MyGardensQueryHookResult = ReturnType<typeof useMyGardensQuery>;
export type MyGardensLazyQueryHookResult = ReturnType<typeof useMyGardensLazyQuery>;
export type MyGardensQueryResult = ApolloReactCommon.QueryResult<MyGardensQuery, MyGardensQueryVariables>;
export const UpdateOneGardenDocument = gql`
    mutation updateOneGarden($id: String!, $name: String, $topSize: Int, $bottomSize: Int, $leftSize: Int, $rightSize: Int, $notes: String, $description: String) {
  updateOneGarden(where: {id: $id}, data: {name: $name, topSize: $topSize, bottomSize: $bottomSize, leftSize: $leftSize, rightSize: $rightSize, notes: $notes, description: $description}) {
    ...GardenFields
  }
}
    ${GardenFieldsFragmentDoc}`;
export type UpdateOneGardenMutationFn = ApolloReactCommon.MutationFunction<UpdateOneGardenMutation, UpdateOneGardenMutationVariables>;
export type UpdateOneGardenComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<UpdateOneGardenMutation, UpdateOneGardenMutationVariables>, 'mutation'>;

    export const UpdateOneGardenComponent = (props: UpdateOneGardenComponentProps) => (
      <ApolloReactComponents.Mutation<UpdateOneGardenMutation, UpdateOneGardenMutationVariables> mutation={UpdateOneGardenDocument} {...props} />
    );
    

/**
 * __useUpdateOneGardenMutation__
 *
 * To run a mutation, you first call `useUpdateOneGardenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateOneGardenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateOneGardenMutation, { data, loading, error }] = useUpdateOneGardenMutation({
 *   variables: {
 *      id: // value for 'id'
 *      name: // value for 'name'
 *      topSize: // value for 'topSize'
 *      bottomSize: // value for 'bottomSize'
 *      leftSize: // value for 'leftSize'
 *      rightSize: // value for 'rightSize'
 *      notes: // value for 'notes'
 *      description: // value for 'description'
 *   },
 * });
 */
export function useUpdateOneGardenMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateOneGardenMutation, UpdateOneGardenMutationVariables>) {
        return ApolloReactHooks.useMutation<UpdateOneGardenMutation, UpdateOneGardenMutationVariables>(UpdateOneGardenDocument, baseOptions);
      }
export type UpdateOneGardenMutationHookResult = ReturnType<typeof useUpdateOneGardenMutation>;
export type UpdateOneGardenMutationResult = ApolloReactCommon.MutationResult<UpdateOneGardenMutation>;
export type UpdateOneGardenMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateOneGardenMutation, UpdateOneGardenMutationVariables>;
export const UpsertOneGardenPixelDocument = gql`
    mutation upsertOneGardenPixel($id: String, $color: String!, $plantCreate: PlantCreateOneWithoutGardenPixelsInput, $plantUpdate: PlantUpdateOneWithoutGardenPixelsInput, $plantable: Boolean!, $plantedDate: DateTime, $gardenId: String!, $plantedByCreate: UserCreateOneWithoutGardenPixelsInput, $plantedByUpdate: UserUpdateOneWithoutGardenPixelsInput, $harvestedDate: DateTime, $notes: String, $x: Int!, $y: Int!) {
  upsertOneGardenPixel(where: {id: $id}, create: {x: $x, y: $y, plant: $plantCreate, color: $color, plantable: $plantable, plantedDate: $plantedDate, garden: {connect: {id: $gardenId}}, plantedBy: $plantedByCreate, harvestedDate: $harvestedDate, notes: $notes}, update: {x: $x, y: $y, plant: $plantUpdate, color: $color, plantable: $plantable, plantedDate: $plantedDate, plantedBy: $plantedByUpdate, harvestedDate: $harvestedDate, notes: $notes}) {
    ...GardenPixelFields
  }
}
    ${GardenPixelFieldsFragmentDoc}`;
export type UpsertOneGardenPixelMutationFn = ApolloReactCommon.MutationFunction<UpsertOneGardenPixelMutation, UpsertOneGardenPixelMutationVariables>;
export type UpsertOneGardenPixelComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<UpsertOneGardenPixelMutation, UpsertOneGardenPixelMutationVariables>, 'mutation'>;

    export const UpsertOneGardenPixelComponent = (props: UpsertOneGardenPixelComponentProps) => (
      <ApolloReactComponents.Mutation<UpsertOneGardenPixelMutation, UpsertOneGardenPixelMutationVariables> mutation={UpsertOneGardenPixelDocument} {...props} />
    );
    

/**
 * __useUpsertOneGardenPixelMutation__
 *
 * To run a mutation, you first call `useUpsertOneGardenPixelMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpsertOneGardenPixelMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [upsertOneGardenPixelMutation, { data, loading, error }] = useUpsertOneGardenPixelMutation({
 *   variables: {
 *      id: // value for 'id'
 *      color: // value for 'color'
 *      plantCreate: // value for 'plantCreate'
 *      plantUpdate: // value for 'plantUpdate'
 *      plantable: // value for 'plantable'
 *      plantedDate: // value for 'plantedDate'
 *      gardenId: // value for 'gardenId'
 *      plantedByCreate: // value for 'plantedByCreate'
 *      plantedByUpdate: // value for 'plantedByUpdate'
 *      harvestedDate: // value for 'harvestedDate'
 *      notes: // value for 'notes'
 *      x: // value for 'x'
 *      y: // value for 'y'
 *   },
 * });
 */
export function useUpsertOneGardenPixelMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpsertOneGardenPixelMutation, UpsertOneGardenPixelMutationVariables>) {
        return ApolloReactHooks.useMutation<UpsertOneGardenPixelMutation, UpsertOneGardenPixelMutationVariables>(UpsertOneGardenPixelDocument, baseOptions);
      }
export type UpsertOneGardenPixelMutationHookResult = ReturnType<typeof useUpsertOneGardenPixelMutation>;
export type UpsertOneGardenPixelMutationResult = ApolloReactCommon.MutationResult<UpsertOneGardenPixelMutation>;
export type UpsertOneGardenPixelMutationOptions = ApolloReactCommon.BaseMutationOptions<UpsertOneGardenPixelMutation, UpsertOneGardenPixelMutationVariables>;
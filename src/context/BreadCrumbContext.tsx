import * as React from 'react';
import { useState } from 'react';

type ICrumbState = {
  name: string;
  location: string;
}[];

type ICrumbContext = [ICrumbState, (state: ICrumbState) => void];

const BreadCrumbContext = React.createContext<ICrumbContext>([
  [],
  (state) => {},
]);

const BreadCrumbProvider = (props: React.Props<{}>) => {
  const [crumbState, setCrumbState] = useState<ICrumbState>([]);

  return (
    <BreadCrumbContext.Provider value={[crumbState, setCrumbState]}>
      {props.children}
    </BreadCrumbContext.Provider>
  );
};

export { BreadCrumbContext, BreadCrumbProvider };
